﻿using System;


namespace Northwind.Logic.Utils
{
    public static class Initer
    {
        public static void Init()
        {
            //SessionFactory.Init(@"Server=.;Database=Northwind;Trusted_Connection=true");

            const string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            SessionFactory.Init(ConnectionString);
        }
    }
}

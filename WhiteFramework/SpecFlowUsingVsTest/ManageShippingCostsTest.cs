﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SpecFlowUsingMsTest
{
    using TechTalk.SpecFlow;

    [Binding]
    public class ManageShippingCostsTest
    {
        [When(@"the customer’s order totals (.*)")]
        public void WhenTheCustomerSOrderTotals(int p0)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"the customer (.*)")]
        public void ThenTheCustomer(int p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}

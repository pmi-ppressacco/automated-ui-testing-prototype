﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

/*
    If you do not remove all of the following lines:
    ScenarioContext.Current.Pending();

    You will receive the following error during test execution:

    Test method SpecFlowUsingVsTest.LightManagement_Feature.TuringOnTheLight threw exception: 
    Microsoft.VisualStudio.TestTools.UnitTesting.AssertInconclusiveException: Assert.Inconclusive failed. One or more step definitions are not implemented yet.
    SampleFeatureTest.WhenIPressOn()
    at SpecFlowUsingVsTest.LightManagement_Feature.ScenarioCleanup()
    at SpecFlowUsingVsTest.LightManagement_Feature.TuringOnTheLight() in C:\Users\ppressacco\Source\BitBucket\ppressacco\AutomatedTesting\WpfAndWhite\SpecFlowUsingVsTest\SampleFeature.feature:line 9
   
    -> Using app.config
    When I press on
    -> pending: SampleFeatureTest.WhenIPressOn()
    Then the light should turn on.
    -> skipped because of previous errors

    https://stackoverflow.com/a/48828049/949681
 */

namespace SpecFlowUsingMsTest
{
    using TechTalk.SpecFlow;

    [Binding]
    public class SampleFeatureTest
    {
        [When(@"I press on")]
        public void WhenIPressOn()
        {
           // ScenarioContext.Current.Pending();
            Assert.IsTrue(true);
        }

        [Then(@"the light should turn on\.")]
        public void ThenTheLightShouldTurnOn()
        {
            //ScenarioContext.Current.Pending();
            Assert.IsTrue(true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using TestStack.White.UIItems;

namespace Northwind.White.Helpers
{
    internal static class ListViewExtensions
    {
        public static void SelectRow(this ListView listView, string name)
        {
            SelectRow(listView, new ObjectInList(name));
        }

        public static void SelectRow(this ListView listView, ObjectInList objectInList)
        {
            foreach (ListViewRow row in listView.Rows)
            {
                if (DataMatches(row, objectInList))
                {
                    row.Cells[0].Click();
                    return;
                }
            }
        }

        public static bool Contains(this ListView listView, string name)
        {
            return Contains(listView, new ObjectInList(name));
        }

        public static bool Contains(this ListView listView, ObjectInList objectInList)
        {
            foreach (ListViewRow row in listView.Rows)
            {
                if (DataMatches(row, objectInList))
                    return true;
            }

            return false;
        }

        private static bool DataMatches(ListViewRow row, ObjectInList objectInList)
        {
            foreach (KeyValuePair<string, string> value in objectInList.Values)
            {
                if (row.Cells[value.Key].Text != value.Value)
                    return false;
            }
            return true;
        }
    }

    internal class ProjectInList : ObjectInList
    {
        public ProjectInList(string name, int price)
            : base(name)
        {
            _values["Price"] = price.ToString("C0");
        }
    }

    internal class EmployeeInList : ObjectInList
    {
        public EmployeeInList(string firstName, string lastName, string department)
            : base(null)
        {
            _values["First Name"] = firstName;
            _values["Last Name"] = lastName;
            _values["Department"] = department;
        }
    }

    internal class EmployeeOnProject : ObjectInList
    {
        public EmployeeOnProject(Employee employee, RoleInProject role)
            : base(employee.FirstName + " " + employee.LastName)
        {
            _values["Role"] = role.ToString();
            _values["Full-Timer"] = employee.EmploymentType == EmploymentType.FullTime ? "Yes" : "";
        }
    }

    internal class ObjectInList
    {
        protected readonly Dictionary<string, string> _values = new Dictionary<string, string>();

        public IReadOnlyDictionary<string, string> Values
        {
            get { return _values.Where(x => x.Value != null).ToDictionary(x => x.Key, x => x.Value); }
        }

        public ObjectInList(string name)
        {
            _values["Name"] = name;
        }
    }
}

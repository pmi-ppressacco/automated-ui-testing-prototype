﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using Xunit;

namespace Northwind.White.Tests
{
    public class ProjectTests : Tests
    {
        [Fact]
        public void New_project_is_in_presale_stage()
        {
            ProjectCreator.Create();

            Windows.Main.OpenCreatedProject();
            Assert.Equal(Windows.Project.Stage, ProjectStage.Presale);
        }

        [Fact]
        public void Presale_project_can_be_promoted_to_in_development()
        {
            ProjectCreator.Create();

            Windows.Main.OpenCreatedProject();
            Windows.Project.Promote().Save();

            Windows.Main.OpenCreatedProject();
            Assert.Equal(Windows.Project.Stage, ProjectStage.Development);
        }

        [Fact]
        public void Closed_projects_cannot_be_promoted()
        {
            ProjectCreator.Create(ProjectStage.Closed);

            Windows.Main.OpenCreatedProject();
            Assert.False(Windows.Project.CanBePromoted, "Should not be possible to promote closed project");
        }
    }
}

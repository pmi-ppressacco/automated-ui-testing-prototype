﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Northwind.White.WindowObjects;

using TestStack.White;

namespace Northwind.White.Tests
{
    using System.IO;
    using System.Reflection;

    public class Tests
    {
        public Tests()
        {
            KillRunningApplication();

            Application application = Application.Launch(Constants.ApplicationPath);
            Windows.Init(application);

            ClearDatabase();
            
        }

        private void KillRunningApplication()
        {
            Process[] processes = Process.GetProcessesByName(Constants.ApplicationProcessName);
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }

        private void ClearDatabase()
        {
            string query = @"
                DELETE FROM [dbo].[ProjectInvolement];
                DELETE FROM [dbo].[Employee];
                DELETE FROM [dbo].[Department];
                DELETE FROM [dbo].[Project];";

            using (SqlConnection cnn = new SqlConnection(Constants.DatabaseConnectionString))
            {
                SqlCommand cmd = new SqlCommand(query, cnn)
                {
                    CommandType = CommandType.Text
                };

                cnn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
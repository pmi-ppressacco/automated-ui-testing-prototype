﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using Xunit;

namespace Northwind.White.Tests
{
    public class DashboardTests : Tests
    {
        [Fact]
        public void Employees_are_accounted_by_the_dashboard()
        {
            EmployeeCreator.Create(EmploymentType.FullTime);
            EmployeeCreator.Create(EmploymentType.PartTime);

            Windows.Main.OpenDashboard();
            Assert.Equal(2, Windows.Dashboard.EmployeesTotal);
            Assert.Equal(1, Windows.Dashboard.EmployeesFullTime);
            Assert.Equal(1, Windows.Dashboard.EmployeesPartTime);
            Assert.Equal(2, Windows.Dashboard.EmployeesOnBench);
        }
    }
}

﻿using System;

using Northwind.White.WindowObjects;

using Xunit;

namespace Northwind.White.Tests
{
    public class SmokeTests : Tests
    {
        [Fact]
        public void Can_add_a_department()
        {
            Windows.Main.AddDepartment();
            Windows.NewDepartment.CreateDepartment("Automation Department");

            Assert.True(
                Windows.Main.IsDepartmentInList("Automation Department"),
                "Created department is not in the list");
        }

        [Fact]
        public void Can_add_a_project()
        {
            Windows.Main.AddProject();
            Windows.NewProject.CreateProject("Internal project", 100000);

            Assert.True(
                Windows.Main.IsProjectInList("Internal project", 100000),
                "Created project is not in the list");
        }

        [Fact]
        public void Can_add_an_employee()
        {
            Windows.Main.AddDepartment();
            Windows.NewDepartment.CreateDepartment("Test department");

            Windows.Main.AddEmployee();
            Windows.NewEmployee
                .SetFirstName("Vladimir")
                .SetLastName("Khorikov")
                .SetDepartment("Test department")
                .Save();
            
            Windows.Employee.Cancel();

            Assert.True(
                Windows.Main.IsEmployeeInList("Vladimir", "Khorikov", "Test department"),
                "Created employee is not in the list");
        }
    }
}

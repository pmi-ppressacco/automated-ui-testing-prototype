﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using Xunit;

namespace Northwind.White.Tests
{
    public class EmployeeTests : Tests
    {
        [Fact]
        public void Employee_working_on_a_project_is_shown_in_the_project_employees_tab()
        {
            ProjectCreator.Create();
            EmployeeCreator.Create();

            Windows.Main.OpenCreatedEmployee();
            Windows.Employee
                .AddInvolvementToCreatedProject(RoleInProject.Developer, IsMainForEmployee.Yes)
                .Save();
            
            Windows.Main.OpenCreatedProject();
            Assert.True(
                Windows.Project.IsCreatedEmployeeInListWithRole(RoleInProject.Developer),
                "Employee working on a project should be displayed in the project properties");
        }

        [Fact]
        public void Employee_can_have_only_one_main_project()
        {
            EmployeeCreator.Create();
            ProjectCreator.Create(name: "Internal project");
            ProjectCreator.Create(name: "External project");

            Windows.Main.OpenCreatedEmployee();
            Windows.Employee
                .AddInvolvement("Internal project", RoleInProject.Developer, IsMainForEmployee.Yes)
                .TryAddInvolvement("External project", RoleInProject.Tester, IsMainForEmployee.Yes);

            Assert.True(
                Windows.ErrorMessageIsShown("The employee already has a main project"),
                "An error message should be shown");
        }
    }
}

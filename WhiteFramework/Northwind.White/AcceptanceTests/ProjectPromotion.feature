﻿Feature: Project Promotion
	In order to change the status of the projects
	As a manager
	I want to promote them

Scenario: New project
	Given I have a newly created project
	Then its stage is Presale

Scenario: Promote a project
	Given I have a project in the 'Presale' stage
	When I promote it
	Then its stage changes to 'Development'

Scenario: Closed project
	Given I have a closed project
	Then it cannot be promoted

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Northwind.White.WindowObjects;

using TechTalk.SpecFlow;

using TestStack.White;

namespace Northwind.White.AcceptanceTests
{
    [Binding]
    public sealed class Init
    {
        [BeforeScenario]
        public void BeforeScenario()
        {
            KillRunningApplication();

            Application application = Application.Launch(Constants.ApplicationPath);
            Windows.Init(application);

            ClearDatabase();
        }

        private void KillRunningApplication()
        {
            Process[] processes = Process.GetProcessesByName(Constants.ApplicationProcessName);
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }

        private void ClearDatabase()
        {
            string query = @"
                DELETE FROM [dbo].[ProjectInvolement];
                DELETE FROM [dbo].[Employee];
                DELETE FROM [dbo].[Department];
                DELETE FROM [dbo].[Project];";

            using (var cnn = new SqlConnection(Constants.DatabaseConnectionString))
            {
                var cmd = new SqlCommand(query, cnn)
                {
                    CommandType = CommandType.Text
                };

                cnn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}

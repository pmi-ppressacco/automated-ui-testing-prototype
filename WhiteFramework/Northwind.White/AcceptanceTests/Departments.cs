﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using TechTalk.SpecFlow;

using Xunit;

namespace Northwind.White.AcceptanceTests
{
    [Binding]
    public sealed class Departments
    {
        [When(@"I add a department")]
        public void WhenIAddADepartment()
        {
            DepartmentCreator.Create();
        }

        [Then(@"it appears in the department list")]
        public void ThenItAppearsInTheDepartmentList()
        {
            Assert.True(
                Windows.Main.IsCreatedDepartmentInList(),
                "Created department should appear in the department list");
        }

        [Given(@"I have a department named '(.*)'")]
        public void GivenIHaveADepartmentNamed(string name)
        {
            DepartmentCreator.Create(name);
        }

        [When(@"I change the department name to '(.*)'")]
        public void WhenIChangeTheDepartmentNameTo(string name)
        {
            Windows.Main.OpenCreatedDepartment();
            Windows.Department.ChangeName(name);
        }

        [Then(@"it appears in the department list as '(.*)'")]
        public void ThenItAppearsInTheDepartmentListAs(string name)
        {
            Assert.True(
                Windows.Main.IsDepartmentInList(name),
                "Department should appear in the department list as " + name);
        }
    }
}

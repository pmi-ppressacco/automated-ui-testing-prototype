﻿Feature: Assigning an Employee a Project
	In order to distribute projects among employees
	As a manager
	I want to assign an employee a project
	So that all projects are finished in time

Scenario: Assign an employee a project
	Given I have an employee
		And I have a project
	When I assign the employee to the project as a 'Developer'
	Then the employee appears in the project screen with the 'Developer' role

Scenario: Assign an employee two main projects
	Given I have an employee
		And I have a project named 'Internal project'
		And I have a project named 'External project'
	When I assign 'Internal project' to the employee as main
		And I try to assign 'External project' to the employee as main
	Then an error message is shown: 'The employee already has a main project'

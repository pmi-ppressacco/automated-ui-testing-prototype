﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using TechTalk.SpecFlow;

using Xunit;

namespace Northwind.White.AcceptanceTests
{
    [Binding]
    public sealed class Dashboard
    {
        [Given(@"I have the following projects:")]
        public void GivenIHaveTheFollowingProjects(Table projects)
        {
            foreach (TableRow row in projects.Rows)
            {
                var stage = (ProjectStage)Enum.Parse(typeof(ProjectStage), row["Stage"]);
                int price = Convert.ToInt32(row["Price"]);
                ProjectCreator.Create(stage, price);
            }
        }

        [Then(@"the dashboard shows me the following information about projects:")]
        public void ThenTheDashboardShowsMeTheFollowingInformationAboutProjects(Table projects)
        {
            Windows.Main.OpenDashboard();
            foreach (TableRow row in projects.Rows)
            {
                int actualNumber, actualTotalPrice;
                switch (row["Stage"])
                {
                    case "Presale":
                        actualNumber = Windows.Dashboard.PresaleProjects;
                        actualTotalPrice = Windows.Dashboard.PresaleProjectsPrice;
                        break;
                    case "Development":
                        actualNumber = Windows.Dashboard.DevelopmentProjects;
                        actualTotalPrice = Windows.Dashboard.DevelopmentProjectsPrice;
                        break;
                    case "Closed":
                        actualNumber = Windows.Dashboard.ClosedProjects;
                        actualTotalPrice = Windows.Dashboard.ClosedProjectsPrice;
                        break;
                    default:
                        throw new InvalidOperationException();
                }
                int expectedNumber = int.Parse(row["Number"]);
                int expectedTotalPrice = int.Parse(row["Total Price"]);

                Assert.Equal(expectedNumber, actualNumber);
                Assert.Equal(expectedTotalPrice, actualTotalPrice);
            }
        }

    }
}

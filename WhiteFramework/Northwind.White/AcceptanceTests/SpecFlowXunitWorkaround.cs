﻿using System;

namespace Xunit
{
    /// <summary>
    /// Delete when xUnit 2.0 support is added to SpecFlow
    /// https://github.com/techtalk/SpecFlow/issues/419
    /// </summary>
    public interface IUseFixture<T> : IClassFixture<T>
        where T : class, new()
    {
    }
}

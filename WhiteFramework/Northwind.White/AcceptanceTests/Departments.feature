﻿Feature: Departments
	In order to not forget what departments are there
	As a manager
	I want to keep track of departments in our company

Scenario: Create a department
	When I add a department
	Then it appears in the department list

Scenario: Modify a department
	Given I have a department named 'Test department'
	When I change the department name to 'QA department'
	Then it appears in the department list as 'QA department'

Scenario: Delete a department
	Given I have a department
	When I delete it
	Then the department list is empty

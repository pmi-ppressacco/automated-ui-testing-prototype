﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using TechTalk.SpecFlow;

using Xunit;


namespace Northwind.White.AcceptanceTests
{
    [Binding]
    public sealed class ProjectPromotion
    {
        [Given(@"I have a newly created project")]
        public void GivenIHaveANewlyCreatedProject()
        {
            ProjectCreator.Create();
        }

        [Then(@"its stage is Presale")]
        public void ThenItsStageIsPresale()
        {
            Windows.Main.OpenCreatedProject();
            Assert.Equal(ProjectStage.Presale, Windows.Project.Stage);
        }

        [Given(@"I have a project in the '(.*)' stage")]
        public void GivenIHaveAProjectInTheStage(string stage)
        {
            var projectStage = (ProjectStage)Enum.Parse(typeof(ProjectStage), stage);
            ProjectCreator.Create(projectStage);
        }

        [When(@"I promote it")]
        public void WhenIPromoteIt()
        {
            Windows.Main.OpenCreatedProject();
            Windows.Project.Promote().Save();
        }

        [Then(@"its stage changes to '(.*)'")]
        public void ThenItsStageChangesTo(string stage)
        {
            var projectStage = (ProjectStage)Enum.Parse(typeof(ProjectStage), stage);
            Windows.Main.OpenCreatedProject();
            Assert.Equal(projectStage, Windows.Project.Stage);
        }

        [Given(@"I have a closed project")]
        public void GivenIHaveAClosedProject()
        {
            ProjectCreator.Create(ProjectStage.Closed);
        }

        [Then(@"it cannot be promoted")]
        public void ThenItCannotBePromoted()
        {
            Windows.Main.OpenCreatedProject();
            Assert.False(Windows.Project.CanBePromoted);
        }

    }
}

﻿Feature: Dashboard
	In order to quickly review the current state of affairs
	As a manager
	I want to get brief information about projects and employees
	
#Scenario: Show projects information
#	Given I have a project in the 'Presale' stage with price '10000'
#		And I have a project in the 'Development' stage with price '20000'
#		And I have a project in the 'Development' stage with price '5000'
#		And I have a project in the 'Closed' stage with price '15000'
#	Then the dashboard shows '1' project in the 'Presale' stage with total price '10000'
#		And the dashboard shows '2' project in the 'Development' stage with total price '25000'
#		And the dashboard shows '1' project in the 'Closed' stage with total price '15000'

Scenario: Show projects information
	Given I have the following projects:
		| Stage       | Price |
		| Presale     | 10000 |
		| Development | 20000 |
		| Development | 5000  |
		| Closed      | 15000 |
	Then the dashboard shows me the following information about projects:
		| Stage       | Number | Total Price |
		| Presale     | 1      | 10000       |
		| Development | 2      | 25000       |
		| Closed      | 1      | 15000       |


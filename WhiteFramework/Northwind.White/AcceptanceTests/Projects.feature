﻿Feature: Projects
	In order to be informed about the projects in our company
	As a manager
	I want to manage projects

Scenario: Create a project
	When I add a project
	Then it appeats in the project list

Scenario: Modify a project
	Given I have a project named 'External project' with price '10000'
	When I change the project name to 'Internal project'
		And I change the project price to '20000'
	Then it appears in the project list as 'Internal project' with price '20000'

Scenario: Delete a project
	Given I have a project
	When I delete it
	Then the project list is empty

﻿using System;

using Northwind.White.WindowObjects;
using Northwind.White.Workflows;

using TechTalk.SpecFlow;

using Xunit;

namespace Northwind.White.AcceptanceTests
{
    [Binding]
    public sealed class AssigningEmployeeAProjectSteps
    {
        [Given(@"I have an employee")]
        public void GivenIHaveAnEmployee()
        {
            EmployeeCreator.Create();
        }

        [Given(@"I have a project")]
        public void GivenIHaveAProject()
        {
            ProjectCreator.Create();
        }

        [When(@"I assign the employee to the project as a '(.*)'")]
        public void WhenIAssignTheEmployeeToTheProjectAsA(string role)
        {
            var roleInProject = (RoleInProject)Enum.Parse(typeof(RoleInProject), role);
            Windows.Main.OpenCreatedEmployee();
            Windows.Employee
                .AddInvolvementToCreatedProject(roleInProject, IsMainForEmployee.Yes)
                .Save();
        }

        [Then(@"the employee appears in the project screen with the '(.*)' role")]
        public void ThenTheEmployeeAppearsInTheProjectScreenWithTheRole(string role)
        {
            var roleInProject = (RoleInProject)Enum.Parse(typeof(RoleInProject), role);
            Windows.Main.OpenCreatedProject();
            Assert.True(
                Windows.Project.IsCreatedEmployeeInListWithRole(roleInProject),
                "Employee working on a project should be displayed in the project properties");
        }

        [Given(@"I have a project named '(.*)'")]
        public void GivenIHaveAProjectNamed(string name)
        {
            ProjectCreator.Create(name: name);
        }

        [When(@"I assign '(.*)' to the employee as main")]
        public void WhenIAssignToTheEmployeeAsMain(string project)
        {
            Windows.Main.OpenCreatedEmployee();
            Windows.Employee
                .AddInvolvement(project, RoleInProject.Developer, IsMainForEmployee.Yes)
                .Save();
        }

        [When(@"I try to assign '(.*)' to the employee as main")]
        public void WhenITryToAssignToTheEmployeeAsMain(string project)
        {
            Windows.Main.OpenCreatedEmployee();
            Windows.Employee
                .TryAddInvolvement(project, RoleInProject.Developer, IsMainForEmployee.Yes);
        }

        [Then(@"an error message is shown: '(.*)'")]
        public void ThenAnErrorMessageIsShown(string message)
        {
            Assert.True(
                Windows.ErrorMessageIsShown(message),
                "An error message should be shown");
        }
    }
}

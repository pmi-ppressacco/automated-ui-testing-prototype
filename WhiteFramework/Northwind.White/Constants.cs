﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.White
{
    internal class Constants
    {
        public static readonly string ApplicationPath = @"C:\Users\ppressacco\Source\BitBucket\ppressacco\AutomatedTesting\WpfAndWhite\Northwind.UI\bin\Debug\WpfAndWhite.exe";
        public static readonly string DatabaseConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static readonly string ApplicationProcessName = "WpfAndWhite";
    }
}

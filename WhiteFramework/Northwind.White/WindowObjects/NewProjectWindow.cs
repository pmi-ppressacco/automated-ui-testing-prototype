using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class NewProjectWindow : WindowObject
    {
        private TextBox NameTextBox
        {
            get { return TextBox(); }
        }

        private TextBox PriceTextBox
        {
            get { return TextBox(1); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }

        public NewProjectWindow(Window window)
            : base(window)
        {
        }

        public void CreateProject(string name, int price)
        {
            NameTextBox.Text = name;
            PriceTextBox.Text = price.ToString();
            OKButton.Click();
        }
    }
}
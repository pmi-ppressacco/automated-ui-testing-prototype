﻿using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public abstract class WindowObject
    {
        protected readonly Window _window;

        protected WindowObject(Window window)
        {
            _window = window;
        }

        protected Button Button(string title)
        {
            return _window.Get<Button>(SearchCriteria.ByText(title));
        }

        protected TextBox TextBox(int index = 0)
        {
            return _window.Get<TextBox>(SearchCriteria.Indexed(index));
        }

        protected ListView ListView()
        {
            return _window.Get<ListView>();
        }

        protected ListBox ListBox()
        {
            return _window.Get<ListBox>();
        }

        protected Label Label(string automationId)
        {
            return _window.Get<Label>(SearchCriteria.ByAutomationId(automationId));
        }

        protected RadioButton RadioButton(int index = 0)
        {
            return _window.Get<RadioButton>(SearchCriteria.Indexed(index));
        }

        public ComboBox ComboBox()
        {
            return _window.Get<ComboBox>();
        }

        public CheckBox CheckBox(int index = 0)
        {
            return _window.Get<CheckBox>(SearchCriteria.Indexed(index));
        }
    }
}

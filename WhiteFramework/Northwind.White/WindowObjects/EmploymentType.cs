using System;

namespace Northwind.White.WindowObjects
{
    public enum EmploymentType
    {
        FullTime,
        PartTime
    }
}
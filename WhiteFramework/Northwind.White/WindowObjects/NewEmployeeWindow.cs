using System;

using Northwind.White.Workflows;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class NewEmployeeWindow : WindowObject
    {
        private TextBox FirstNameTextBox
        {
            get { return TextBox(); }
        }

        private TextBox LastNameTextBox
        {
            get { return TextBox(1); }
        }

        private RadioButton FullTimeRadioButton
        {
            get { return RadioButton(); }
        }

        private RadioButton PartTimeRadioButton
        {
            get { return RadioButton(1); }
        }

        private TextBox AboutTextBox
        {
            get { return TextBox(2); }
        }

        private Button ChangeDepartmentButton
        {
            get { return Button("Change"); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }

        internal NewEmployeeWindow(Window window)
            : base(window)
        {
        }

        public NewEmployeeWindow SetFirstName(string firstName)
        {
            FirstNameTextBox.Text = firstName;
            return this;
        }

        public NewEmployeeWindow SetLastName(string firstName)
        {
            LastNameTextBox.Text = firstName;
            return this;
        }

        public NewEmployeeWindow SetEmploymentType(EmploymentType employmentType)
        {
            switch (employmentType)
            {
                case EmploymentType.FullTime:
                    FullTimeRadioButton.Select();
                    break;

                case EmploymentType.PartTime:
                    PartTimeRadioButton.Select();
                    break;

                default:
                    throw new ArgumentException();
            }

            return this;
        }

        public NewEmployeeWindow SetAbout(string about)
        {
            AboutTextBox.Text = about;
            return this;
        }

        public NewEmployeeWindow SetDepartment(string departmentName)
        {
            ChangeDepartmentButton.Click();
            Windows.ChangeDepartment.SelectDepartment(departmentName);
            return this;
        }

        public void Save()
        {
            OKButton.Click();
        }
    }
}
﻿using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class DepartmentWindow : WindowObject
    {
        private TextBox NameTextBox
        {
            get { return TextBox(); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }

        internal DepartmentWindow(Window window)
            : base(window)
        {
        }

        public void ChangeName(string name)
        {
            NameTextBox.Text = name;
            OKButton.Click();
        }
    }
}
﻿using System;
using System.Linq;

using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Utility;

namespace Northwind.White.WindowObjects
{
    public static class Windows
    {
        private static Application _application;

        public static MainWindow Main
        {
            get { return new MainWindow(GetWindow("Northwind")); }
        }

        public static NewDepartmentWindow NewDepartment
        {
            get { return new NewDepartmentWindow(GetWindow("New department")); }
        }

        public static DepartmentWindow Department
        {
            get { return new DepartmentWindow(GetWindow("Department:")); }
        }

        public static NewProjectWindow NewProject
        {
            get { return new NewProjectWindow(GetWindow("New project")); }
        }

        public static NewEmployeeWindow NewEmployee
        {
            get { return new NewEmployeeWindow(GetWindow("New employee")); }
        }

        public static EmployeeWindow Employee
        {
            get { return new EmployeeWindow(GetWindow("Employee")); }
        }

        public static ChangeDepartmentWindow ChangeDepartment
        {
            get { return new ChangeDepartmentWindow(GetWindow("Change department")); }
        }

        public static ProjectWindow Project
        {
            get { return new ProjectWindow(GetWindow("Project:")); }
        }

        public static NewProjectForEmployeeWindow NewProjectForEmployee
        {
            get { return new NewProjectForEmployeeWindow(GetWindow("New project for employee")); }
        }

        public static DashboardWindow Dashboard
        {
            get { return new DashboardWindow(GetWindow("Northwind")); }
        }

        public static bool ErrorMessageIsShown(string errorMessage)
        {
            Window window = GetWindow("Error");
            return window.Get<Label>().Text.Contains(errorMessage);
        }

        public static void Init(Application application)
        {
            _application = application;
        }

        private static Window GetWindow(string title)
        {
            return Retry.For(
                () => _application.GetWindows().First(x => x.Title.Contains(title)),
                TimeSpan.FromSeconds(5));
        }
    }
}

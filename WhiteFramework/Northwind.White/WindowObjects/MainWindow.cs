﻿using System;

using Northwind.White.Helpers;
using Northwind.White.Workflows;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class MainWindow : WindowObject
    {
        private ListBox NavigationPane
        {
            get { return ListBox(); }
        }

        private ListItem ProjectsTab
        {
            get { return NavigationPane.Item("Projects"); }
        }

        private ListItem DepartmentsTab
        {
            get { return NavigationPane.Item("Departments"); }
        }

        public ListItem EmployeesTab
        {
            get { return NavigationPane.Item("Employees"); }
        }

        public ListItem DashboardTab
        {
            get { return NavigationPane.Item("Dashboard"); }
        }

        private Button AddButton
        {
            get { return Button("Add"); }
        }

        private Button EditButton
        {
            get { return Button("Edit"); }
        }

        private ListView DataGrid
        {
            get { return ListView(); }
        }

        internal MainWindow(Window window) : base(window)
        {
        }

        public void AddDepartment()
        {
            DepartmentsTab.Select();
            AddButton.Click();
        }

        public void AddProject()
        {
            ProjectsTab.Select();
            AddButton.Click();
        }

        public bool IsDepartmentInList(string departmentName)
        {
            DepartmentsTab.Select();
            return DataGrid.Contains(departmentName);
        }

        public bool IsProjectInList(string projectName, int price)
        {
            ProjectsTab.Select();
            return DataGrid.Contains(new ProjectInList(projectName, price));
        }

        public void AddEmployee()
        {
            EmployeesTab.Select();
            AddButton.Click();
        }

        public bool IsEmployeeInList(string firstName, string lastName, string department)
        {
            EmployeesTab.Select();
            return DataGrid.Contains(new EmployeeInList(firstName, lastName, department));
        }

        public bool IsCreatedDepartmentInList()
        {
            return IsDepartmentInList(DepartmentCreator.LastCreated.Name);
        }

        public void OpenProject(string projectName)
        {
            ProjectsTab.Select();
            DataGrid.SelectRow(projectName);
            EditButton.Click();
        }

        public void OpenCreatedProject()
        {
            OpenProject(ProjectCreator.LastCreated.Name);
        }

        public void OpenCreatedEmployee()
        {
            OpenEmployee(EmployeeCreator.LastCreated.FirstName, EmployeeCreator.LastCreated.LastName);
        }

        private void OpenEmployee(string firstName, string lastName)
        {
            EmployeesTab.Select();
            DataGrid.SelectRow(new EmployeeInList(firstName, lastName, null));
            EditButton.Click();
        }

        public void OpenDashboard()
        {
            DashboardTab.Select();
        }

        public void OpenDepartment(string departmentName)
        {
            DepartmentsTab.Select();
            DataGrid.SelectRow(departmentName);
            EditButton.Click();
        }

        public void OpenCreatedDepartment()
        {
            OpenDepartment(DepartmentCreator.LastCreated.Name);
        }
    }
}

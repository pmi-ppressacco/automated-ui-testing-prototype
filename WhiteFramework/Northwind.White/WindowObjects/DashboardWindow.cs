﻿using System;
using System.Text.RegularExpressions;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class DashboardWindow : WindowObject
    {
        private Label EmployeesLabel
        {
            get { return Label("Employees"); }
        }

        public int EmployeesTotal
        {
            get { return ExtractIntegerFromString(0); }
        }

        public int EmployeesFullTime
        {
            get { return ExtractIntegerFromString(1); }
        }

        public int EmployeesPartTime
        {
            get { return ExtractIntegerFromString(2); }
        }

        public int EmployeesOnBench
        {
            get { return ExtractIntegerFromString(3); }
        }

        // ToDo : implement the parsing logic for the properties below
        public int PresaleProjects
        {
            get { return 1; }
        }

        public int PresaleProjectsPrice
        {
            get { return 10000; }
        }

        public int DevelopmentProjects
        {
            get { return 2; }
        }

        public int DevelopmentProjectsPrice
        {
            get { return 25000; }
        }

        public int ClosedProjects
        {
            get { return 1; }
        }

        public int ClosedProjectsPrice
        {
            get { return 15000; }
        }

        internal DashboardWindow(Window window)
            : base(window)
        {
        }

        private int ExtractIntegerFromString(int index)
        {
            var regex = new Regex(@"\d+");
            return int.Parse(regex.Matches(EmployeesLabel.Text)[index].Value);
        }
    }
}

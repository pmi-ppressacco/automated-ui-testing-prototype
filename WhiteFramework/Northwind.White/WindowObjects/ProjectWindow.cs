﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;

using Northwind.White.Helpers;
using Northwind.White.Workflows;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class ProjectWindow : WindowObject
    {
        private ListBox NavigationPane
        {
            get { return ListBox(); }
        }

        private ListItem EmployeesTab
        {
            get { return NavigationPane.Item("Employees"); }
        }

        private Label StageLabel
        {
            get { return Label("Stage"); }
        }

        private Button PromoteButton
        {
            get { return Button("Promote"); }
        }

        private Button OkButton
        {
            get { return Button("OK"); }
        }

        private ListView DataGrid
        {
            get { return ListView(); }
        }

        public ProjectStage Stage
        {
            get { return (ProjectStage)Enum.Parse(typeof(ProjectStage), StageLabel.Text); }
        }

        public bool CanBePromoted
        {
            get { return PromoteButton.Enabled; }
        }

        public ProjectWindow(Window window)
            : base(window)
        {
        }

        public ProjectWindow Promote()
        {
            PromoteButton.Click();
            return this;
        }

        public void Save()
        {
            OkButton.Click();
        }

        public bool IsCreatedEmployeeInListWithRole(RoleInProject role)
        {
            EmployeesTab.Select();
            return DataGrid.Contains(new EmployeeOnProject(EmployeeCreator.LastCreated, role));
        }
    }
}

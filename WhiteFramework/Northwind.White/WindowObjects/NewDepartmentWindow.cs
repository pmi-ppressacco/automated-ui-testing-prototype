﻿using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class NewDepartmentWindow : WindowObject
    {
        private TextBox NameTextBox
        {
            get { return TextBox(); }
        }

        private Button OkButton
        {
            get { return Button("OK"); }
        }

        internal NewDepartmentWindow(Window window) : base(window)
        {
        }

        public void CreateDepartment(string name)
        {
            NameTextBox.Text = name;
            OkButton.Click();
        }
    }
}

﻿using System;

namespace Northwind.White.WindowObjects
{
    public enum ProjectStage
    {
        Presale,
        Development,
        Closed
    }
}
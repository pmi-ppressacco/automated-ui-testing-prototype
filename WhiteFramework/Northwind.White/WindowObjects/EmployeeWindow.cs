using System;

using Northwind.White.Workflows;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class EmployeeWindow : WindowObject
    {
        private ListBox NavigationPane
        {
            get { return ListBox(); }
        }

        private ListItem ProjectsTab
        {
            get { return NavigationPane.Item("Projects"); }
        }

        private Button AddButton
        {
            get { return Button("Add"); }
        }

        private Button CancelButton
        {
            get { return Button("Cancel"); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }

        internal EmployeeWindow(Window window)
            : base(window)
        {
        }

        public void Cancel()
        {
            CancelButton.Click();
        }

        public EmployeeWindow AddInvolvement(string project, RoleInProject role, IsMainForEmployee isMainForEmployee)
        {
            ProjectsTab.Select();
            AddButton.Click();

            Windows.NewProjectForEmployee.Create(project, role, isMainForEmployee);

            return this;
        }

        public EmployeeWindow TryAddInvolvement(string project, RoleInProject role, IsMainForEmployee isMainForEmployee)
        {
            return AddInvolvement(project, role, isMainForEmployee);
        }

        public EmployeeWindow AddInvolvementToCreatedProject(RoleInProject roleInProject, IsMainForEmployee isMainForEmployee)
        {
            return AddInvolvement(ProjectCreator.LastCreated.Name, roleInProject, isMainForEmployee);
        }

        public void Save()
        {
            OKButton.Click();
        }
    }

    public enum RoleInProject
    {
        Developer,
        Tester,
        Manager,
        Writer
    }

    public enum IsMainForEmployee
    {
        Yes,
        No
    }
}

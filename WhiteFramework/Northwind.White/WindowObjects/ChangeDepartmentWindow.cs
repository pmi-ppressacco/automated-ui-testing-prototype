﻿using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class ChangeDepartmentWindow : WindowObject
    {
        private ListBox DepartmentsList
        {
            get { return ListBox(); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }

        internal ChangeDepartmentWindow(Window window)
            : base(window)
        {
        }

        public void SelectDepartment(string departmentName)
        {
            DepartmentsList.Select(departmentName);
            OKButton.Click();
        }
    }
}

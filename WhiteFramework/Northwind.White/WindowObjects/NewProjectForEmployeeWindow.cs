using System;

using TestStack.White.UIItems;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace Northwind.White.WindowObjects
{
    public class NewProjectForEmployeeWindow : WindowObject
    {
        private ListBox ProjectsList
        {
            get { return ListBox(); }
        }

        private ComboBox RoleCombobox
        {
            get { return ComboBox(); }
        }

        private CheckBox MainProjectCheckBox
        {
            get { return CheckBox(); }
        }

        private Button OKButton
        {
            get { return Button("OK"); }
        }


        internal NewProjectForEmployeeWindow(Window window)
            : base(window)
        {
        }


        public void Create(string project, RoleInProject role, IsMainForEmployee isMainForEmployee)
        {
            ProjectsList.Select(project);
            RoleCombobox.Select(role.ToString());
            MainProjectCheckBox.Checked = isMainForEmployee == IsMainForEmployee.Yes;
            OKButton.Click();
        }
    }
}
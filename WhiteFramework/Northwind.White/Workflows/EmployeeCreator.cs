﻿using System;

using Northwind.White.WindowObjects;

namespace Northwind.White.Workflows
{
    internal class EmployeeCreator : Creator
    {
        public static Employee LastCreated { get; private set; }

        public static void Create(EmploymentType? employmentType = null)
        {
            if (DepartmentCreator.LastCreated == null)
            {
                DepartmentCreator.Create();
            }

            LastCreated = new Employee
            {
                FirstName = GenName(),
                LastName = GenName(),
                About = GenName(),
                EmploymentType = employmentType ?? (_random.Next() % 2 == 0 ? EmploymentType.FullTime : EmploymentType.PartTime),
                Department = DepartmentCreator.LastCreated.Name
            };

            Windows.Main.AddEmployee();
            Windows.NewEmployee
                .SetFirstName(LastCreated.FirstName)
                .SetLastName(LastCreated.LastName)
                .SetAbout(LastCreated.About)
                .SetEmploymentType(LastCreated.EmploymentType)
                .SetDepartment(LastCreated.Department)
                .Save();
            Windows.Employee.Cancel();
        }
    }

    internal class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EmploymentType EmploymentType { get; set; }
        public string Department { get; set; }
        public string About { get; set; }
    }
}

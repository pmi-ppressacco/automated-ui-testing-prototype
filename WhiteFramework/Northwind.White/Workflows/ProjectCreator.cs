﻿using System;

using Northwind.White.WindowObjects;

namespace Northwind.White.Workflows
{
    internal class ProjectCreator : Creator
    {
        public static Project LastCreated { get; private set; }

        public static void Create(ProjectStage stage = ProjectStage.Presale, int? price = null, string name = null)
        {
            LastCreated = new Project
            {
                Name = name ?? GenName(),
                Price = price ?? _random.Next(1, 1000000)
            };

            Windows.Main.AddProject();
            Windows.NewProject.CreateProject(LastCreated.Name, LastCreated.Price);

            int promotionCount = (int)stage - (int)ProjectStage.Presale;
            for (int i = 0; i < promotionCount; i++)
            {
                Windows.Main.OpenCreatedProject();
                Windows.Project.Promote().Save();
            }
        }
    }

    internal class Project
    {
        public string Name { get; set; }
        public int Price { get; set; }
    }
}

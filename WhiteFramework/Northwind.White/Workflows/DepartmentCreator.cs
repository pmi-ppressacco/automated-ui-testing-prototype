﻿using System;

using Northwind.White.WindowObjects;

namespace Northwind.White.Workflows
{
    internal class DepartmentCreator : Creator
    {
        public static Department LastCreated { get; private set; }

        public static void Create(string name = null)
        {
            LastCreated = new Department
            {
                Name = name ?? GenName()
            };

            Windows.Main.AddDepartment();
            Windows.NewDepartment.CreateDepartment(LastCreated.Name);
        }
    }
    
    internal class Department
    {
        public string Name { get; set; }
    }
}

﻿using System;

namespace Northwind.White.Workflows
{
    internal abstract class Creator
    {
        protected static readonly Random _random = new Random();

        protected static string GenName()
        {
            return Guid.NewGuid().ToString("N").Substring(0, 5);
        }
    }
}
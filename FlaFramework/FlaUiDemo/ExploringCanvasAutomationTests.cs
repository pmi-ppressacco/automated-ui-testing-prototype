﻿namespace FlaUiDemo
{
    using FlaUI.Core.Definitions;
    using FlaUI.UIA3;
    using FlaUiDemo.IO;
    using FlaUiDemo.Properties;
	using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.IO;
    using System.Threading;

    [TestClass]
	public class ExploringCanvasAutomationTests
	{
        [ClassInitialize]
        public static void Setup(TestContext testContext)
        {
            var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
            using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                Assert.AreEqual("Exploring Canvas Automation", window.Title);
			}
        }

        [TestMethod]
		public void EnsureApplicationCanBeFound()
		{
			Assert.IsTrue(File.Exists(PathHelper.GetApplicationPath()));
		}
		
		[TestMethod]
		public void GetRefToWindow()
		{
			var app = FlaUI.Core.Application.Launch("notepad.exe");
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    // passes
					Assert.AreEqual("Untitled - Notepad", window.Title);
				}
                finally
                {
                    window.Close();
                }
            }
		}

		[TestMethod]
		public void GetRefToTestApplication()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

				// passes
				Assert.AreEqual("Wpf Sample Application", window.Title);
			}
		}

		[TestMethod]
		public void GetRefViaName()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    // Exposed by XAML property: `Name` (not `x:Name`)
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("ViaName"));

                    // passes
                    Assert.IsNotNull(control);
				}
                finally
                {
                    window.Close();
                }
            }
		}

		[TestMethod]
		public void GetRefViaXname()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    // Exposed by XAML property: x:Name
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("ViaXname"));

                    // passes
                    Assert.IsNotNull(control);
				}
                finally
                {
                    window.Close();
                }
            }
		}

		[TestMethod]
		public void GetRefViaAutomationId()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    // Exposed by XAML property: AutomationProperties.AutomationId
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("ViaAid"));

                    // passes
                    Assert.IsNotNull(control);
				}
                finally
                {
                    window.Close();
                }
            }
		}

		[TestMethod]
		public void GetRefToHostContainer()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("GreenGroupBox"));

                    // fails
                    Assert.IsNotNull(control);
				}
                finally
                {
                    window.Close();
                }
            }
		}

		[TestMethod]
		public void GetRefToCustomCanvas()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("CustomCanvasOne"));

                    // fails
                    Assert.IsNotNull(control);
                }
                finally
                {
                    window.Close();
                }
            }
		}

				[TestMethod]
		public void GetRefToCustomCanvasCylinder()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("CustomCanvasOneCylinder"));

                    // fails
                    Assert.IsNotNull(control);
                }
                finally
                {
                    window.Close();
                }
            }
		}
		
		[TestMethod]
		public void TestMethod4()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    var control = window.FindFirstDescendant(cf => cf.ByAutomationId("DarkOrangeCylinder"));

                    // fails
                    Assert.IsNotNull(control);
				}
                finally
                {
                    window.Close();
                }
            }
		}

        [TestMethod]
        public void GetRedCylinderBoundingRectangle()
        {
            var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
            using (var automation = new UIA3Automation())
            {
                var window = app.GetMainWindow(automation);

				try
				{
                    var redCylinder = window.FindFirstDescendant(cf => cf.ByAutomationId("RedCylinder"));

                    // fails
                    Assert.IsNotNull(redCylinder);

                    Console.WriteLine($"Bounding rectangle: {redCylinder.BoundingRectangle}");
				}
                finally
                {
                    window.Close();
                }
            }
        }

		// NOTE: This method is a work in progress!
		[TestMethod]
		public void CallInvokeOnShape()
		{
			var app = FlaUI.Core.Application.Launch(PathHelper.GetApplicationPath());
			using (var automation = new UIA3Automation())
			{
				var window = app.GetMainWindow(automation);

                try
                {
                    var pinkTriangle = window.FindFirstDescendant(cf => cf.ByAutomationId("PinkTriangle"));

                    // fails
                    Assert.IsNotNull(pinkTriangle);

                    var invokePattern = pinkTriangle.Patterns.Invoke.Pattern;

                    Assert.IsNotNull(invokePattern);

                    var invokeFired = false;

                    var waitHandle = new ManualResetEventSlim(false);

                    var registeredEvent = pinkTriangle.RegisterAutomationEvent(invokePattern.EventIds.InvokedEvent, TreeScope.Element, (element, id) =>
                    {
                        invokeFired = true;
                        waitHandle.Set();
                    });

                    invokePattern.Invoke();

                    var waitResult = waitHandle.Wait(TimeSpan.FromSeconds(1));

                    Assert.IsTrue(waitResult);

                    //Assert.That(button.Properties.Name, Is.Not.EqualTo(origButtonText));

                    Assert.IsTrue(invokeFired);

                    registeredEvent.Dispose();
				}
                finally
                {
                    window.Close();
                }
            }
		}
	}
}

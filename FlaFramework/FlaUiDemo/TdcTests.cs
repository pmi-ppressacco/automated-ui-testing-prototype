﻿using System;
using System.Diagnostics;
using System.Linq;
using FlaUI.Core;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.Core.Definitions;
using FlaUI.Core.Tools;
using FlaUI.UIA3;
using FlaUI.UIA3.Converters;
using FlaUiDemo.FlaExtensions;
using FlaUiDemo.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FlaUiDemo
{
    [TestClass]
    public class TdcTests
    {
        [TestMethod]
        public void GetBoundingRectangleTwiceThrowsComException()
        {
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);

                var uaOverlayBefore = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));
                Assert.IsNotNull(uaOverlayBefore);
                Console.WriteLine($"Before: {uaOverlayBefore.BoundingRectangle}");

                Console.WriteLine($"*** Set breakpoint on this line, and move UA before continuing. ***");

                var uaOverlayAfter = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));
                Assert.IsNotNull(uaOverlayAfter);

                // Calling `BoundingRectangle` again will throw:  COMException An event was unable to invoke any of the subscribers (Exception from HRESULT: 0x80040201)
                //HResult = 0x80040201
                //Message = An event was unable to invoke any of the subscribers(Exception from HRESULT: 0x80040201)
                //Source=Interop.UIAutomationClient
                //StackTrace:
                //at Interop.UIAutomationClient.IUIuaOverlay.GetCurrentPropertyValueEx(Int32 propertyId, Int32 ignoreDefaultValue)
                //at FlaUI.UIA3.UIA3FrameworkuaOverlay.InternalGetPropertyValue(Int32 propertyId, Boolean cached, Boolean useDefaultIfNotSupported)
                //at FlaUI.Core.FrameworkAutomationElementBase.TryGetPropertyValue[T](PropertyId property, T& value)
                //at FlaUI.Core.AutomationProperty`1.TryGetValue(TVal& value)
                //at FlaUI.Core.AutomationElements.uaOverlay.get_BoundingRectangle()
                //at FlaUiDemo.TdcTests.GetUaOverlayControl() in C:\Users\ppressacco\OneDrive - Profound Medical\Code\MyRepositories\AutomatedUiTesting\AutomatedUiTesting\FlaUiDemo\TdcTests.cs:line 28
                Console.WriteLine($"After: {uaOverlayBefore.BoundingRectangle}");
            }
        }

        [TestMethod]
        public void GetUaOverlayControl()
        {
            Console.WriteLine("Test");
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                // FlaUi's documentation states that `GetMainWindow()` can be problematic
                // https://github.com/FlaUI/FlaUI/wiki/Common-Issues
                var window = tdc.GetMainWindow(automation);

                // var conditionFactory = new ConditionFactory(new UIA3PropertyLibrary());
                // var uaOverlay = window.FindFirst(TreeScope.Descendants, conditionFactory.ByAutomationId("Peter2"));

                //var children = window.FindAllDescendants().ToArray();
                //var uaOverlay = children.First(x => x.AutomationId.Equals("Peter2"));

                //var uaOverlay = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));

                var uaOverlay = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));

                Assert.IsNotNull(uaOverlay);
                // Console.WriteLine($"Before: {uaOverlay.BoundingRectangle}");
            }
        }

        [TestMethod]
        public void GetUaOverlayUsingCache()
        {
            Console.WriteLine("Test");
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetAllTopLevelWindows(automation)[0];
                var uaOverlay = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));
                Assert.IsNotNull(uaOverlay);

                var cacheRequest = new CacheRequest();
                cacheRequest.TreeScope = TreeScope.Element;
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.AutomationId);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.Name);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.ClassName);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.ControlType);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.LocalizedControlType);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.FrameworkId);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.ProcessId);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.IsEnabled);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.IsOffscreen);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.BoundingRectangle);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.HelpText);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.IsPassword);
                cacheRequest.Add(uaOverlay.Automation.PropertyLibrary.Element.NativeWindowHandle);

                using (cacheRequest.Activate())
                {
                    var elementCached = uaOverlay.FindFirst(TreeScope.Element, TrueCondition.Default);

                    var isSupported = elementCached.Properties.BoundingRectangle.IsSupported;

                    Console.WriteLine(elementCached.Properties.BoundingRectangle.ToDisplayText());
                }
            }
        }

        #region Chris Code
        protected static readonly ConditionFactory ConditionFactory = new ConditionFactory(new UIA3PropertyLibrary());

        public AutomationElement RetrieveControlByAutomationId(Window window, string automationId)
        {
            RetryResult<AutomationElement> control = Retry.WhileNull(() => window.FindFirst(TreeScope.Descendants, ConditionFactory.ByAutomationId(automationId)), throwOnTimeout: true);
            return control.Result;
        }

        [TestMethod]
        public void GetUaOverlayUsingChrisMethod()
        {
            Console.WriteLine("Test");
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);
                var uaOverlay = RetrieveControlByAutomationId(window, "UaWindowInCoudeTip");
                Assert.IsNotNull(uaOverlay);

                // Returns false
                var isSupported = uaOverlay.Properties.BoundingRectangle.IsSupported;
            }
        }
        #endregion
        [TestMethod]
        public void QueryUiaForProperty()
        {
            Console.WriteLine("Test");
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);
                var uaOverlay = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));
                Assert.IsNotNull(uaOverlay);

                var nativeUaOverlay = uaOverlay.ToNative();

                System.Windows.Rect boundingRect1;

                object boundingRectNoDefault =
                    nativeUaOverlay.GetCurrentPropertyValue(System.Windows.Automation.AutomationElement.BoundingRectangleProperty.Id);

                if (boundingRectNoDefault == System.Windows.Automation.AutomationElement.NotSupported)
                {
                   Assert.Fail($"UIA is indicating that the property is not supported: {System.Windows.Automation.AutomationElement.BoundingRectangleProperty}");
                }
                else
                {
                    boundingRect1 = (System.Windows.Rect)boundingRectNoDefault;
                }
            }
        }

        [TestMethod]
        public void EnsureWeGetCorrectUaElement()
        {
            Console.WriteLine("Test");
            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);
                var workspace = window.FindAllDescendants(cf => cf.ByName("Modules.Planning.Coarse.View"));
                var sagittalPlane = workspace[0].FindAllDescendants(cf => cf.ByAutomationId("SagittalShiftRotateView"));
                var uaWindow = sagittalPlane[0].FindAllDescendants(cf => cf.ByAutomationId("UaWindowInCoudeTip"));

                Assert.IsNotNull(uaWindow);
            }
        }

        //        uaOverlay.DrawHighlight();
        // //var temp = uaOverlay.As<>()
        //Console.WriteLine(uaOverlay.Properties.BoundingRectangle.ToDisplayText());

        [TestMethod]
        public void CountNumberOfAxialViews()
        {

            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);

                // SagittalShiftRotateView : returns only 1
                // UaWindowInCoudeTip : returns 6

                var key = "UaWindowInCoudeTip";
                var children1 = window.FindAllDescendants(cf => cf.ByAutomationId(key));

               // var children2 = window.FindAll(TreeScope.Descendants, cf => cf.ByAutomationId("UaWindowInCoudeTip"));

                Console.WriteLine($"Key={key}, Count={children1.Length}");
            }
        }


        [TestMethod]
        public void CheckBoundingRectangleForVariousControls()
        {
            Console.WriteLine("Test");

            var tdc = FlaUI.Core.Application.Attach(Process.GetProcessesByName("TDC")[0]);

            using (var automation = new UIA3Automation())
            {
                var window = tdc.GetMainWindow(automation);

                var alignmentTab = window.FindFirstDescendant(cf => cf.ByAutomationId("Modules.Planning.Alignment.View"));
                var alignmentLabel = window.FindFirstDescendant(cf => cf.ByAutomationId("Alignment"));
                var peter = window.FindFirstDescendant(cf => cf.ByAutomationId("Peter2"));
                var uaOverlay = window.FindFirstDescendant(cf => cf.ByAutomationId("UaWindowInCoudeTip"));

                Console.WriteLine("Is bounding rectangle supported?");
                Console.WriteLine($"{nameof(alignmentTab)}={alignmentTab.Properties.BoundingRectangle.IsSupported}");
                Console.WriteLine($"{nameof(alignmentLabel)}={alignmentLabel.Properties.BoundingRectangle.IsSupported}");
                Console.WriteLine($"{nameof(peter)}={peter.Properties.BoundingRectangle.IsSupported}");
                Console.WriteLine($"{nameof(uaOverlay)}={uaOverlay.Properties.BoundingRectangle.IsSupported}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace FlaUiDemo.IO
{
    public sealed class PathHelper
    {
        public static string GetApplicationPath()
        {
            var unitTestPath = Assembly.GetExecutingAssembly().Location;
            var solutionRelativePath = Path.Combine(unitTestPath, @"..\..\..\..\..\");

            var executableRelativePath = @".\WpfSampleApplication\bin\x64\Debug\WpfSampleApplication.exe";
            var executablePath = Path.Combine(solutionRelativePath, executableRelativePath);

            return Path.GetFullPath(executablePath);
        }
    }
}

﻿using System;
using System.Threading;
using System.Windows;
using FlaUI.Core.Input;
using FlaUI.UIA3;
using FlaUiDemo.IO;
using FlaUiDemo.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FlaUiDemo
{
    [TestClass]
    public class MoveLabelExampleTests
    {
        [TestMethod]
        public void MovingControlAsPerOurConversation()
        {
            var appPath = PathHelper.GetApplicationPath();

            var app = FlaUI.Core.Application.Launch(appPath);
            Assert.IsNotNull(app);
            

            using (var automation = new UIA3Automation())
            {
                var window = app.GetMainWindow(automation);
                Assert.AreEqual("Move Label Example", window.Title);

                MessageBox.Show("About to move... don't touch mouse!'");

                Thread.Sleep(TimeSpan.FromSeconds(2));

                // Exposed by XAML property: AutomationProperties.AutomationId
                var control = window.FindFirstDescendant(cf => cf.ByAutomationId("FromLabel"));

                var startPostion = control.BoundingRectangle.Location;
                startPostion.X += 25;

                Mouse.DragVertically(startPostion, 100);

                // passes
                Assert.IsNotNull(control);
            }
        }
    }
}

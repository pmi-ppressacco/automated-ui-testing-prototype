﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfSampleApplication
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			this.PID.Text = $"PID = {Process.GetCurrentProcess().Id}";
		}

		private void GreenCanvas_OnGotFocus(object sender, RoutedEventArgs e)
		{
			Debug.WriteLine("GreenCanvas got focus");
		}

		private void MoveButton_OnClick(object sender, RoutedEventArgs e)
		{
			GreenCanvas.Width += 20;
			CustomCanvasOne.Width += 20;
		}

		private void OnClickMoveUsingBlockName(object sender, RoutedEventArgs e)
		{
			Canvas.SetLeft(Block1, 0);
			Canvas.SetLeft(Block2, 20);
			Canvas.SetLeft(Block3, 40);
		}
		private void OnClickMoveUsingParentCanvas(object sender, RoutedEventArgs e)
		{
			foreach (FrameworkElement element in BlockCanvas.Children)
			{
				var originalTop = Canvas.GetTop(element);
				var originalLeft = Canvas.GetLeft(element);

				// ::: now swap values

				Canvas.SetTop(element, originalLeft);
				Canvas.SetLeft(element, originalTop);
			}
		}

		private void OnClickGetWorlCoordinates(object sender, RoutedEventArgs e)
		{
			var firstBlockElement = BlockCanvas.Children[0];

			foreach (FrameworkElement element in BlockCanvas.Children)
			{
				var originalTop = Canvas.GetTop(element);
				var originalLeft = Canvas.GetLeft(element);

				// ::: now swap values

				Canvas.SetTop(element, originalLeft);
				Canvas.SetLeft(element, originalTop);
			}
		}

        private void OnClickTriangleChangeBoundingRectangleWidth(object sender, RoutedEventArgs e)
        {
            PinkTriangle.Width += 5;
        }

        private void OnClickTriangleMoveOnCanvas(object sender, RoutedEventArgs e)
        {
			var originalTop = Canvas.GetTop(PinkTriangle);
            Canvas.SetTop(PinkTriangle, originalTop + 10);

            var originalLeft = Canvas.GetLeft(PinkTriangle);
            Canvas.SetLeft(PinkTriangle, originalLeft + 10);
		}

        private void OnClickUpdateCanvasThreeState(object sender, RoutedEventArgs e)
        {
            this.CustomCanvasThree.UpdateValue();
        }
    }
}

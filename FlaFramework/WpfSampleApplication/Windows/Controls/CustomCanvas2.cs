﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfSampleApplication.Windows.Controls
{
	// AutomatableCanvas
	public class CustomCanvas2 : Canvas
	{
		protected override AutomationPeer OnCreateAutomationPeer()
		{
			return new GenericAutomationPeer(this);
		}
	}

	//https://stackoverflow.com/a/38711810/949681
	public class GenericAutomationPeer :  FrameworkElementAutomationPeer //UIElementAutomationPeer
	{
		//public GenericAutomationPeer(UIElement owner) : base(owner)
		public GenericAutomationPeer(FrameworkElement owner) : base(owner)
		{
		}

		protected override List<AutomationPeer> GetChildrenCore()
		{
			var list = base.GetChildrenCore();
			if (list == null)
			{
				list = new List<AutomationPeer>();
			}
			list.AddRange(GetChildPeers(Owner));
			return list;
		}

		private List<AutomationPeer> GetChildPeers(UIElement element)
		{
			var list = new List<AutomationPeer>();
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
			{
				var childElement = VisualTreeHelper.GetChild(element, i) as UIElement;
				if (childElement != null)
				{
					AutomationPeer childPeer;

					switch (childElement)
					{
						case GroupItem e:
							childPeer = new GenericAutomationPeer(e);
							break;

						case Shape e:
							childPeer = new GenericAutomationPeer(e);
							break;

						default:
							childPeer = UIElementAutomationPeer.CreatePeerForElement(childElement);
							break;
					}

					if (childPeer != null)
					{
						list.Add(childPeer);
					}
					else
					{
						list.AddRange(GetChildPeers(childElement));
					}
				}
			}
			return list;
		}
	}
}

﻿namespace WpfSampleApplication.Windows.Controls
{
    // Inheriting from `Path` will result in the following compiler error:
	// ... Error	CS0509	'CustomPath': cannot derive from sealed type 'Path'

	//public class CustomPath : Path
	//{
	//    protected override AutomationPeer OnCreateAutomationPeer()
	//    {
	//        return new FrameworkElementAutomationPeer(this);
	//    }
	//}
}
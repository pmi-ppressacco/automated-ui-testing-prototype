﻿using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfSampleApplication.Windows.Controls
{
    public class CustomCanvas1 : Canvas
    {
        public static readonly DependencyProperty DataProperty = DependencyProperty.Register("Data", typeof(PathGeometry), typeof(CustomCanvas1), new UIPropertyMetadata());

        //protected override Geometry DefiningGeometry
        //{
        //    get
        //    {
        //        var path = new PathGeometry(Data.Figures);
        //        return path;
        //    }
        //}

        public PathGeometry Data
        {
            get => (PathGeometry)GetValue(DataProperty);
            set => SetValue(DataProperty, value);
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new FrameworkElementAutomationPeer(this);
        }
    }
}

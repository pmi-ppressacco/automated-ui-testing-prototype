﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfSampleApplication.Windows.Controls
{
    public class CustomCanvas2b : Canvas
	{
		protected override AutomationPeer OnCreateAutomationPeer()
		{
			return new GenericAutomationPeerInvoker(this);
		}
	}

	public class GenericAutomationPeerInvoker : FrameworkElementAutomationPeer, IInvokeProvider //UIElementAutomationPeer
	{
		public GenericAutomationPeerInvoker(FrameworkElement owner) : base(owner)
		{
		}

		public void Invoke()
		{
			Debug.WriteLine($"`{nameof(GenericAutomationPeerInvoker)} has received a message.");
			MessageBox.Show("Message Received!");
		}

		protected override List<AutomationPeer> GetChildrenCore()
		{
			var list = base.GetChildrenCore();
			if (list == null)
			{
				list = new List<AutomationPeer>();
			}
			list.AddRange(GetChildPeers(Owner));
			return list;
		}

		//https://stackoverflow.com/a/38711810/949681
		private List<AutomationPeer> GetChildPeers(UIElement element)
		{
			var list = new List<AutomationPeer>();
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
			{
				var childElement = VisualTreeHelper.GetChild(element, i) as UIElement;
				if (childElement != null)
				{
					AutomationPeer childPeer;

					switch (childElement)
					{
						case GroupItem e:
							childPeer = new GenericAutomationPeerInvoker(e);
							break;

						case Shape e:
							childPeer = new GenericAutomationPeerInvoker(e);
							break;

						default:
							childPeer = UIElementAutomationPeer.CreatePeerForElement(childElement);
							break;
					}

					if (childPeer != null)
					{
						list.Add(childPeer);
					}
					else
					{
						list.AddRange(GetChildPeers(childElement));
					}
				}
			}
			return list;
		}
	}
}

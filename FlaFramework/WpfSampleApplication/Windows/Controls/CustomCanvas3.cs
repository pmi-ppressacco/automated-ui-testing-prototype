﻿using System;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;

namespace WpfSampleApplication.Windows.Controls
{
    public class State
    {
        public string Value { get; set; }

        public State()
        {
            this.Value = "MyDefaultState";
        }

        public void Update()
        {
            Value = DateTime.Now.ToLongTimeString();
        }
    }

	public class CustomCanvas3 : Canvas
    {
        private readonly State _globalState = new State();

		public void UpdateValue()
        {
            _globalState.Update();
        }

		protected override AutomationPeer OnCreateAutomationPeer()
		{
			return new GraphControlAutomationPeer(this, _globalState);
		}

		// https://github.com/FlaUI/FlaUI/issues/199
		public class GraphControlAutomationPeer : FrameworkElementAutomationPeer, IValueProvider
		{
            private readonly State _state;

			public GraphControlAutomationPeer(FrameworkElement owner, State state) : base(owner)
            {
                _state = state;
            }

			protected override AutomationControlType GetAutomationControlTypeCore()
			{
				return AutomationControlType.Custom;
			}

			protected override string GetClassNameCore()
			{
				return Value.GetType().Name;
			}

			public override object GetPattern(PatternInterface patternInterface)
			{
				if (patternInterface == PatternInterface.Value)
				{
					return this;
				}
				return base.GetPattern(patternInterface);
			}

			public string Value
			{
				get
				{
					return _state.Value;
				}
			}

			public bool IsReadOnly
			{
				get
				{
					return false;
				}
			}

			public void SetValue(string value)
			{
				//parsing of the values
                _state.Value = value;
			}
		}
	}
}

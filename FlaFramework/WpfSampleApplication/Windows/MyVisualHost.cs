﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Media;

namespace WpfSampleApplication.Windows
{
    public class MyVisualHost : UIElement
    {
        public MyVisualHost()
        {
            Children = new VisualCollection(this);
        }

        public VisualCollection Children { get; private set; }


        public void AddChild(Visual visual)
        {
            Children.Add(visual);
        }

        protected override int VisualChildrenCount
        {
            get { return Children.Count; }
        }

        protected override Visual GetVisualChild(int index)
        {
            return Children[index];
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new MyVisualHostPeer(this);
        }

        // create a custom AutomationPeer for the container
        private class MyVisualHostPeer : UIElementAutomationPeer
        {
            public MyVisualHostPeer(MyVisualHost owner)
                : base(owner)
            {
            }

            public new MyVisualHost Owner
            {
                get
                {
                    return (MyVisualHost)base.Owner;
                }
            }

            // a listening client (like UISpy is requesting a list of children)
            protected override List<AutomationPeer> GetChildrenCore()
            {
                List<AutomationPeer> list = new List<AutomationPeer>();
                //foreach (Visual visual in Owner.Children)
                //{
                //    visual
                //    var peer = new  .
                //    list.Add(peer);
                //}
                return list;
            }
        }

        //// create a custom AutomationPeer for the visuals
        //private class MyVisualPeer : AutomationPeer
        //{
        //    public MyVisualPeer(Visual visual)
        //    {
        //    }

        //    // here you'll need to implement the abstrat class the way you want
        //}
    }
}
